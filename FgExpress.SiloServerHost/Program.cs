using System;
using System.IO;
using System.Reflection;
using Orleans;
using Orleans.Runtime.Configuration;
using Orleans.Runtime.Host;


namespace FgExpress.SiloServerHost
{
    /// <summary>
    /// Orleans test silo host
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {
            //var path = GetAssemblyPath();
            //var configFilePath = Path.Combine(path, "OrleansConfiguration.xml");
            //var fileInfo = new FileInfo(configFilePath);
            //var para = new Dictionary<string, string>();
            //para.Add("ConnectionString", "mongodb://127.0.0.1:27017/");
            //para.Add("Database", "OrleansState");
            //siloConfig.Globals.RegisterStorageProvider<MongoDBStorage>("MongoDBStore", para);




            var siloConfig = ClusterConfiguration.LocalhostPrimarySilo();
            siloConfig.StandardLoad();

            var silo = new SiloHost("EdwardSilo", siloConfig);

            //silo.Config.Globals.LivenessType = GlobalConfiguration.LivenessProviderType.Custom;
            //silo.Config.Globals.MembershipTableAssembly = "OrleansConsulUtils";
            //silo.Config.Globals.ReminderServiceType = GlobalConfiguration.ReminderServiceProviderType.Disabled;

            silo.InitializeOrleansSilo();
            silo.StartOrleansSilo();

            Console.WriteLine("Silo started.");
            // Then configure and connect a client.
            var clientConfig = ClientConfiguration.LocalhostSilo();
            var client = new ClientBuilder().UseConfiguration(clientConfig).Build();
            client.Connect().Wait();

            Console.WriteLine("Client connected.");

            Console.WriteLine("\nPress Enter to terminate...");
            Console.ReadLine();
            // Shut down
            client.Close();
            silo.ShutdownOrleansSilo();
        }


        private static string GetAssemblyPath()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var codeBaseUri = new UriBuilder(codeBase);
            var pathString = Uri.UnescapeDataString(codeBaseUri.Path);
            return Path.GetDirectoryName(pathString);
        }
    }
}
