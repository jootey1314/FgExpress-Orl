﻿using Orleans;
using Orleans.Runtime.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using FgExpress.SiloIGrain;
using FgExpress.Models;
using System.Threading.Tasks;

namespace FgExpress.Apis.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public async Task <IEnumerable<string>> Get()
        {
            var path = GetAssemblyPath();
            var configFilePath = Path.Combine(path, "ClientConfiguration.xml");
            GrainClient.Initialize(configFilePath);
            IOrder orderTask = GrainClient.GrainFactory.GetGrain<IOrder>("jooper");
            var p = await orderTask.Get();
            return new string[] { p };
        }

        // GET api/values/5
        public async Task<string> Get(int id)
        {
            //GrainClient.Initialize(ClientConfiguration.LoadFromFile(@".\ClientConfiguration.xml"));
            var path = GetAssemblyPath();
            var configFilePath = Path.Combine(path, "ClientConfiguration.xml");
            GrainClient.Initialize(configFilePath);


            var playerId = "jooper";
            IOrder orderTask = GrainClient.GrainFactory.GetGrain<IOrder>(playerId);
            var user = new User{ Name="jooper" };
            await orderTask.Add(user);
            var result = await orderTask.Get();
            return result;
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        private static string GetAssemblyPath()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var codeBaseUri = new UriBuilder(codeBase);
            var pathString = Uri.UnescapeDataString(codeBaseUri.Path);
            return Path.GetDirectoryName(pathString);
        }
    }
}
