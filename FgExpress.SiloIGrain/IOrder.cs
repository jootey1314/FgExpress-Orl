using System.Threading.Tasks;
using Orleans;
using FgExpress.Models;

namespace FgExpress.SiloIGrain
{
    /// <summary>
    /// Grain interface IGrain1
    /// </summary>
    public interface IOrder : IGrainWithStringKey
    {
        Task Add(User user);

        Task<string> Get();
    }
}
