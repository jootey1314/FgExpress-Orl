using System.Threading.Tasks;
using Orleans;
using FgExpress.SiloIGrain;
using Orleans.Providers;
using FgExpress.Models;

namespace FgExpress.SiloGrains
{
    /// <summary>
    /// Grain implementation class Grain1.MemoryStore
    /// </summary>

    
    [StorageProvider(ProviderName = "MongoDBStore")]
    public class Order : Grain<User>, IOrder
    {
        public async Task Add(User user)
        {
            await Task.Factory.StartNew(() => {
                this.State.Name = user.Name;
            });
        }


        public async Task<string> Get()
        {
            return await Task.Factory.StartNew(() =>
            {
                return this.State.Name;
            });
        }
    }
}
